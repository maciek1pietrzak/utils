package utils

import (
	"time"
	"math/rand"
)

type randomGen struct {
	rnd *rand.Rand
}

var rndGen = NewRandomGen(rand.NewSource(time.Now().Unix()))

func NewRandomGen(s rand.Source) randomGen {
	return randomGen{rand.New(s)}
}

func RandomFloat(min, max float64) float64 {
	return min + rndGen.rnd.Float64()*((max-min)+1)
}

func Float64() float64 {
	return rndGen.rnd.Float64()
}

func RandomInt(min, max int) int {
	return min + rndGen.rnd.Intn(max+1-min)
}
